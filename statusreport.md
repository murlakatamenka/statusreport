# Status check (:+1:)

- [Status check (:+1:)](#status-check-1)
    - [Duty](#duty)
    - [Userscore](#userscore)

---

## Duty

Time || Cat | Dog
:---:|:---|:---:|:---:
Saturday | | | |
| | morning | ✔️ | ✔️ |
| | afternoon | | ✔️ |
| | evening | ✔️ | ✔️ |
Sunday | | | |
| | morning | | |
| | afternoon | | |
| | evening | | |
| --- ||||
| **Last updated on** | `2018-07-29 03:13:34` |||

---

## Userscore

User | Score
--- | ---
me | `2`
mom | `1.5`
dad | `1.5`
